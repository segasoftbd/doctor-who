﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctorWho
{
    public class Episode
    {
        public string SeriesId { get; set; }
        public string Series { get; set; }
        public string Year { get; set; }
        public string Name { get; set; }
    }
}
