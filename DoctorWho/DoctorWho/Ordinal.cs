﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctorWho
{
    public static class Ordinal
    {
        //public  string Generate(int num)
        //{
        //    if (num <= 0) return num.ToString();

        //    switch (num % 100)
        //    {
        //        case 11:
        //        case 12:
        //        case 13:
        //            return num + "th";
        //    }

        //    switch (num % 10)
        //    {
        //        case 1:
        //            return num + "st";
        //        case 2:
        //            return num + "nd";
        //        case 3:
        //            return num + "rd";
        //        default:
        //            return num + "th";
        //    }

        //}

        //public static string ToOrdinal(this long number)
        //{
        //    if (number < 0) return number.ToString();
        //    long rem = number % 100;
        //    if (rem >= 11 && rem <= 13) return number + "th";

        //    switch (number % 10)
        //    {
        //        case 1:
        //            return number + "st";
        //        case 2:
        //            return number + "nd";
        //        case 3:
        //            return number + "rd";
        //        default:
        //            return number + "th";
        //    }
        //}

        //public static string ToOrdinal(this int number)
        //{
        //    return ((long)number).ToOrdinal();
        //}

        public static string ToOrdinal(this string number)
        {
            if (String.IsNullOrEmpty(number)) return number;

            var dict = new Dictionary<string, string>();
            dict.Add("0", "Zeroth");
            dict.Add("1", "First");
            dict.Add("2", "Second");
            dict.Add("3", "Third");
            dict.Add("4", "Fourth");
            dict.Add("5", "Fifth");
            dict.Add("6", "Sixth");
            dict.Add("7", "Seventh");
            dict.Add("8", "Eighth");
            dict.Add("9", "Ninth");
            dict.Add("10", "Tenth");
            dict.Add("11", "Eleventh");
            dict.Add("12", "Twelfth");
            dict.Add("13", "Thirteenth");
            dict.Add("14", "Fourteenth");
            dict.Add("15", "Fifteenth");
            dict.Add("16", "Sixteenth");
            dict.Add("17", "Seventeenth");
            dict.Add("18", "Eighteenth");
            dict.Add("19", "Nineteenth");
            dict.Add("20", "Twentieth");

            // rough check whether it's a valid number
            string temp = number.ToLower().Trim().Replace(" and ", " ");
            string[] words = temp.Split(new char[] { ' ', '-' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string word in words)
            {
                if (!dict.ContainsKey(word)) return number;
            }

            // extract last word
            number = number.TrimEnd().TrimEnd('-');
            int index = number.LastIndexOfAny(new char[] { ' ', '-' });
            string last = number.Substring(index + 1);

            // make replacement and maintain original capitalization
            if (last == last.ToLower())
            {
                last = dict[last];
            }
            else if (last == last.ToUpper())
            {
                last = dict[last.ToLower()].ToUpper();
            }
            else
            {
                last = last.ToLower();
                last = Char.ToUpper(dict[last][0]) + dict[last].Substring(1);
            }

            return number.Substring(0, index + 1) + last;
        }

    }
}
