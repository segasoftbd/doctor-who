﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctorWho
{
    public class Companion
    {
        public string SeriesId { get; set; }
        public string Series { get; set; }
        public string Name { get; set; }
    }
}
