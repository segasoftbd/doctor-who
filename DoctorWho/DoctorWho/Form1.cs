﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DoctorWho
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// public list of doctor,companion, episode
        /// </summary>

        List<Doctor> DoctorList = new List<Doctor>();
        List<Companion> CompanionList = new List<Companion>();
        List<Episode> EpisodeList = new List<Episode>();

        /// <summary>
        /// Open txt file and load data into list 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;
            //initialize file dialog
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                //set filter it allows txt file only
                openFileDialog.Filter = "txt files (*.txt)|*.txt";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //after opening file this read all lines
                    string[] filelines = File.ReadAllLines(openFileDialog.FileName);
                    //we start count from 3, index 3=E
                    for(int i=3;i<filelines.Length;i++)
                    {
                        if(filelines[i].Substring(0,1)=="E")
                        {
                            string [] delimiters = filelines[i].Split('|');
                            EpisodeList.Add(new Episode { SeriesId=delimiters[1].Trim(),Series=delimiters[2].Trim(), Year=delimiters[3].Trim(), Name=delimiters[4].Trim() });
                        }
                        else if (filelines[i].Substring(0, 1) == "C")
                        {
                            string[] delimiters = filelines[i].Split('|');
                            CompanionList.Add(new Companion {Name=delimiters[1].Trim() + "("+delimiters[2].Trim() + ")",Series=delimiters[3].Trim(), SeriesId=delimiters[4].Trim() });
                        }
                        else if (filelines[i].Substring(0, 1) == "D")
                        {
                            string[] delimiters = filelines[i].Split('|');
                            DoctorList.Add(new Doctor {OrderId=Convert.ToInt32(delimiters[1].Trim()), OrderedNo= Ordinal.ToOrdinal(delimiters[1].Trim()), Name=delimiters[2].Trim(), Series=delimiters[3].Trim(), Age=delimiters[4].Trim(), SeriesId=delimiters[5].Trim() });
                        }
                    }
                    //finish to grab data into list
                    //now load item into control

                    LoadData();
                }
            }

        }
        /// <summary>
        /// This exit the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        /// <summary>
        /// Load data into combobox control
        /// </summary>
        public void LoadData()
        {
            DoctorList = DoctorList.OrderBy(x => x.OrderId).ToList();

            //insert item in combobox
            foreach(var item in DoctorList)
            {
                cmbDoctor.Items.Add(item.OrderedNo);
            }
            cmbDoctor.SelectedIndex = 0;
        }
        /// <summary>
        /// set data into another control depending on combobox value
        /// </summary>
        public void setData()
        {
            //retive orderNo
            var doctor_OrderedNo = cmbDoctor.Text;
            //load doctor info
            var doctorInfo = DoctorList.FirstOrDefault(x => x.OrderedNo == doctor_OrderedNo);
            var seriesId = doctorInfo.SeriesId;
            var episodeList = EpisodeList.Where(x => x.SeriesId == seriesId).ToList();
            var companionList = CompanionList.Where(x => x.Series == doctorInfo.OrderId.ToString()).ToList();

            txtAge.Text = doctorInfo.Age;
            txtSeries.Text = doctorInfo.Series;
            txtYear.Text = episodeList.FirstOrDefault().Year;
            txtPlayedby.Text = doctorInfo.Name;
            txtEpName.Text = episodeList.FirstOrDefault().Name;
            listBox1.Items.Clear();
            foreach(var item in companionList)
            {
                
                var episode = EpisodeList.FirstOrDefault(x => x.SeriesId == item.SeriesId);
                if(episode!=null)
                {
                    listBox1.Items.Add(item.Name);
                    listBox1.Items.Add('"'+episode.Name +'"'+ "(" + episode.Year + ")");
                    listBox1.Items.Add("");
                }
                else
                {
                    listBox1.Items.Add(item.Name);
                    listBox1.Items.Add("");
                }
                
            }




        }
        /// <summary>
        /// call setData() function when combobox item is being changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbDoctor_SelectedIndexChanged(object sender, EventArgs e)
        {
            setData();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
