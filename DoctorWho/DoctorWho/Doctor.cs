﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctorWho
{
    public class Doctor
    {
        public int OrderId { get; set; }
        public string OrderedNo { get; set; }
        public string Name { get; set; }
        public string SeriesId { get; set; }
        public string Series { get; set; }
        public string Age { get; set; }
    }
}
